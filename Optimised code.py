import random
print("ROCK PAPER SCISSORS")
map = {0: "rock", 1: "paper", 2: "scissors"}
table = [[-1, 1, 0], [1, -1, 2], [0, 2, -1]]
history=dict()
ppoints=0
cpoints=0
winner=None
r=1
def choice(r):
    print("Round ", r )
    print("Enter your choice:")
    uchoice = input()
    return uchoice
for m in range(10):
    uchoice = choice(r)
    cchoice=random.randint(0,2)
    if uchoice.lower() == "rock": move = 0
    elif uchoice.lower() == "paper": move = 1
    elif uchoice.lower() == "scissors": move = 2
    else:
          print("Wrong Input!!")
          uchoice =choice(r)
    winner = table[move][cchoice]
    history[r] = (uchoice, map[cchoice], winner)
    if winner== move: ppoints += 10
    else: cpoints += 10
    r += 1
print("\nComputer Score", cpoints)
print("Player Score", ppoints)
print("Enter the round for which you need the information:")
n=int(input())
h, c, a = history[n]
print("Player choice=", h)
print("Computer choice=", c)
if a == -1: print("Round",n,"was a tie")
elif a == move:print(" Player won Round ",n)
elif a == cchoice: print("Computer won Round ", n)
else: print("Wrong Inputs")
