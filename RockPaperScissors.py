import random


def points(Computer, Player, c, p):                                 #Function to calculate points in each round
    comp = 0
    play = 0
    if Computer == "Rock" and Player == "S":
        c += 5
        comp = 1
    elif Computer == "Scissors" and Player == "R":
        p += 5
        play = 1
    elif Computer == "Scissors" and Player == "P":
        c += 5
        comp = 1
    elif Player == "S" and Computer == "Paper":
        p += 5
        play = 1
    elif Computer == "Paper" and Player == "R":
        c += 5
        comp = 1
    elif Player == "P" and Computer == "Rock":
        p += 5
        play = 1
    return [c, p, comp, play]


Round_dict = {}                                                     #To find the winner of each round
computer_dict = {}                                                  #To store computer's choice in each round
player_dict = {}                                                    #To store player's choice in each round
Points = []                                                         #List to store points
Comp_choices = ["Rock", "Paper", "Scissors"]
print("\t\t\t\t\t\t\t\t\t\tROCK-PAPER-SCISSORS")
print("Use R for Rock \n    P for Paper \n    S for Scissors\n")
Computer_points = 0
Player_points = 0

for i in range(10):                                                #Loop for 10 rounds
    Computer = Comp_choices[random.randint(0, 2)]
    print("Round", i+1)
    Player = input("Enter your choice: (R, P, S)?  ")
    Points = points(Computer, Player, Computer_points, Player_points)            #function call to calculate points
    Computer_points = Points[0]
    Player_points = Points[1]
    if Player == "R":
       player_dict[i+1] = "Rock"
    elif Player == "P":
        player_dict[i + 1] = "Paper"
    elif Player == "S":
        player_dict[i + 1] = "Scissors"
    computer_dict[i + 1] = Computer
    if Points[2] == 1:
        Round_dict[i + 1] = "COMPUTER"
    elif Points[3] == 1:
        Round_dict[i + 1] = "PLAYER"
    else:
        Round_dict[i + 1] = 0

print("\t\t\t\t\t\t\t\t\t\tSCORES")                              #To display score
print("Player's score:", Player_points)
print("Computer's score:", Computer_points)
if Computer_points > Player_points:
    print("CONGRATULATIONS!! COMPUTER WON THE GAME")
elif Computer_points == Player_points:
    print("THE GAME WAS A TIE!!")
else:
    print("CONGRATULATIONS!! PLAYER WON THE GAME")

print("\nEnter the round for which you need the information: ")       #To fetch each round's info
x = int(input())
if x > 10:
    print("SORRY!! THE GAME HAS ONLY 10 ROUNDS")
else:
    print("Player's choice: ", player_dict[x])
    print("Computer's choice: ", computer_dict[x])
    if Round_dict[x] == 0:
        print("ROUND ", x, " WAS A TIE")
    else:
        print(Round_dict[x], " WON ROUND ", x)
